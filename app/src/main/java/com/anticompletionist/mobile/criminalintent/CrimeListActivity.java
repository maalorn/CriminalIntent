package com.anticompletionist.mobile.criminalintent;

import android.support.v4.app.Fragment;

/**
 * Created by maalorn on 5/8/2016.
 */
public class CrimeListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new CrimeListFragment();
    }
}
